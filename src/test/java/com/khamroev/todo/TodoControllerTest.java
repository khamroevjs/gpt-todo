package com.khamroev.todo;

import com.khamroev.todo.controller.TodoController;
import com.khamroev.todo.model.Todo;
import com.khamroev.todo.repository.TodoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TodoController.class)
public class TodoControllerTest {

    @MockBean
    private TodoRepository todoRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllTodos() throws Exception {
        List<Todo> todos = new ArrayList<>();
        todos.add(new Todo(1L, "Task 1", "Description 1"));
        todos.add(new Todo(2L, "Task 2", "Description 2"));

        when(todoRepository.findAll()).thenReturn(todos);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/todos")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void testGetTodoById() throws Exception {
        Long todoId = 1L;
        Todo todo = new Todo(todoId, "Task 1", "Description 1");

        when(todoRepository.findById(todoId)).thenReturn(Optional.of(todo));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/todos/{id}", todoId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("title").value("Task 1"))
                .andExpect(jsonPath("description").value("Description 1"));
    }

    @Test
    public void testCreateTodo() throws Exception {
        Todo todoToCreate = new Todo();
        todoToCreate.setId(1L);
        todoToCreate.setTitle("Task 1");
        todoToCreate.setDescription("Description 1");

        when(todoRepository.save(any(Todo.class))).thenReturn(todoToCreate);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"title\":\"Task 1\",\"description\":\"Description 1\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("title").value("Task 1"))
                .andExpect(jsonPath("description").value("Description 1"));

        verify(todoRepository, times(1)).save(any(Todo.class));
    }

    @Test
    public void testUpdateTodo() throws Exception {
        long todoId = 1L;
        Todo updatedTodo = new Todo(todoId, "Updated Task", "Updated Description");

        when(todoRepository.existsById(todoId)).thenReturn(true);
        when(todoRepository.save(any(Todo.class))).thenReturn(updatedTodo);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/todos/{id}", todoId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"title\":\"Updated Task\",\"description\":\"Updated Description\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("title").value("Updated Task"))
                .andExpect(jsonPath("description").value("Updated Description"));

        verify(todoRepository, times(1)).save(any(Todo.class));
    }

    @Test
    public void testUpdateTodoWhenIdNotFound() throws Exception {
        Long nonExistentTodoId = 999L;

        when(todoRepository.existsById(nonExistentTodoId)).thenReturn(false);

        mockMvc.perform(put("/api/todos/{id}", nonExistentTodoId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"title\":\"Updated Task\",\"description\":\"Updated Description\"}"))
                .andExpect(status().isNotFound());

        verify(todoRepository, never()).save(any(Todo.class));
    }


    @Test
    public void testDeleteTodo() throws Exception {
        Long todoId = 1L;

        when(todoRepository.existsById(todoId)).thenReturn(true);

        mockMvc.perform(delete("/api/todos/{id}", todoId))
                .andExpect(status().isNoContent());

        verify(todoRepository, times(1)).deleteById(todoId);
    }

    @Test
    public void testDeleteTodoWhenIdNotFound() throws Exception {
        Long nonExistentTodoId = 999L;

        when(todoRepository.existsById(nonExistentTodoId)).thenReturn(false);

        mockMvc.perform(delete("/api/todos/{id}", nonExistentTodoId))
                .andExpect(status().isNotFound());

        verify(todoRepository, never()).deleteById(nonExistentTodoId);
    }

}
