# Use an OpenJDK runtime as a parent image
FROM amazoncorretto:17-alpine AS builder

# Set the working directory
WORKDIR /app

# Copy the JAR file into the container at /app
COPY build/libs/todo-1.0.jar /app

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Run the JAR file
CMD ["java", "-jar", "todo-1.0.jar"]
