#!/bin/bash

# Step 1: Build the project using Gradle
echo "Step 1: Building the project using Gradle"
gradle bootJar

# Check if the build was successful
if [ $? -eq 0 ]; then
  echo "Build successful"
else
  echo "Build failed. Exiting."
  exit 1
fi

# Step 2: Deploy the project using Docker Compose
echo "Step 2: Deploying the project using Docker Compose"
docker-compose up -d

# Check if the deployment was successful
if [ $? -eq 0 ]; then
  echo "Deployment successful"
else
  echo "Deployment failed. Exiting."
  exit 1
fi

echo "Project deployment is complete."
